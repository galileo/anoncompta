#!/bin/bash

strFICHIER=$1

echo $strFICHIER

IFS=$'\n'       # make newlines the only separator
for j in `cat $strFICHIER`
do
	DATE=`echo $j | awk -F";" '{ print $1 }'`
	BORD=`echo $j | awk -F";" '{ print $2 }'`
        PIEC=`echo $j | awk -F";" '{ print $3 }'`
        DESI=`echo $j | awk -F";" '{ print $4 }'`
        IMPU=`echo $j | awk -F";" '{ print $5 }'`
        TIER=`echo $j | awk -F";" '{ print $6 }'`
        #NUME=`echo $j | awk -F";" '{ print $7 }'`
        MBUD=`echo $j | awk -F";" '{ print $7 }'`
        MTTC=`echo $j | awk -F";" '{ print $8 }'`

	echo $DESI|egrep "FACTURE CANTINE |FACTURE GARDERIE " > /dev/null
	if [ $? -eq 0 ]; then
		#echo Cest une facture anonymisation
		TIER=`echo $TIER|sha1sum`
	fi

	echo "$DATE;$BORD;$PIEC;$DESI;$IMPU;$TIER;$MBUD;$MTTC"
done

