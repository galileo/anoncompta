# Description
Traitement de l'extraction des données au format SLK

# Transformation du fichier SLK en fichier csv
Afin d'être traité par le script d'anonymisation , le fichier doit être converti au format csv

## Encodage
Le fichier est issu d'un poste de travail au format WINDOWS, il faut le convertire en utf8

```bash
iconv -f iso-8859-1 -t UTF-8  ../compta/Edition\ du\ grand\ livre\,\ détail\ par\ article.SLK > temp.SLK
```

## Convertion
En une ligne de commande grace à libreoffice sans intercation utilisateur

```bash
libreoffice --infilter="CSV:44,34,UTF-8,1,,UTF-8" --headless --convert-to csv temp.SLK 
```

libreoffice : la célèbre suite bureautique libre et respectueuse de la vie privée

 --infilter="CSV:44,34,UTF-8,1,,UTF-8" : 

   44 = ASCII code of »,«

   34 = ASCII code of »"«

   UTF-8 = input encoding

   1 = Start in line 1 = first line

   Empty column options = import all as “Standard” formatted

   UTF-8 = output encoding

 --headless : Pas d'interaction avec l'utilisateur

 --convert-to csv : le format de fichier cible 

```bash
convert Edition du grand livre, détail par article.SLK -> Edition du grand livre, détail par article.csv using filter : Text - txt - csv (StarCalc)
```

## Anonymisation

Utilser le script PYTHON

```bash
python anoncompta.py temp.csv
```

