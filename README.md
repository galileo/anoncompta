
# Description
script d'anonymisation d'un livre de compte

# Contexte
Suite à la demande d'un citoyen d'une commune d'obtenir le grand livre de compte, le service comptable de la municipalité s'est apperçu que cette extraction nécessitait une anonymisation.

# Données disponibles
L'extraction a été réalisée au format PDF, un export au format CSV faciliterait l'exploitation.

# Transformation du fichier PDF
Pour permettre au script de traiter le fichier, il faut le convertir au format csv

## Etape 1
Dans un premier temps, le document PDF est convertit au format TXT

```bash
 pdftotext -layout GrandLivre_2015_25-03-2016.pdf etape1.txt
```
le paramètre -layout est important afin que les espaces dans une ligne ne soient pas considérés comme des retour chariot.

## Etape 2
La deuxième étape consiste à remplacer les espaces multiples par un caractère de séparation utilisé dans les fichiers CSV ("," ou ";" ou "tabulation")
ici c'est le caractère ";" qui est choisi car il n'est pas utilisé sur les lignes comportants des écritures comptables.

```bash
cat etape1.txt |sed -re 's/((D|R) [0-9]{1,5}\/[0-9]{1,3})/          \1/g'|sed 's/ \+ /;/g' > etape2.txt
```
le premier sed permet de séparer une désignation longue d'une imputation

## Etape 3
La troisième étape consiste à rechercher uniquement les lignes possédant une date d'écriture et un montant en euros

```bash
cat etape2.txt |grep "€"|egrep "(([0-9]{1,2}/){2})2015"  > final.txt
```
# Finalisation
Les fichier est prêt pour un traitement par le script d'anonymisation.
Les étapes nécessaires pour la transformation d'un fichier PDF en fichier CSV sont peu nombreuses et rapides mais elles peuvent êtres sources d'erreur.
Une ligne corectement construite commence par une date et se termine par le symbole euro "€"
```bash
cat GrandLivre_2015_25-03-2016.txt |egrep  "^ (([0-9]{1,2}/){2})2015"|wc -l
5380
cat GrandLivre_2015_25-03-2016.txt |egrep  "^ (([0-9]{1,2}/){2})2015"|egrep "€$"|wc -l
4756
```
Un peu plus de 11% des lignes non pas pus être correctement interprétées. Cela n'es pas bloquant pour la création du script d'anonmisation.

Seule un export, à partir du logiciel de comptabilité, dans un format assurant l'interopérabilité permet de garantir l'exactitude des informations traitées.

# Anonymisation
Le traitement de 4756 lignes a duré environ 2 minutes.


